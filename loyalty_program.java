import java.util.*;

public class MyClass {
    public static void main(String args[]) {
  
        Scanner sc= new Scanner(System.in);
        System.out.print("input poin: ");  
        int poin = sc.nextInt();
        
        List<String> voucherList = new ArrayList<>();
        
        while(poin>=100){
            
            if(poin>=800){
                int j = poin/800;
                for(int i=0;i<j;i++){
                    voucherList.add("100rb");
                }
                poin = poin%800;
            }
            
            if(poin>=400){
                int j = poin/400;
                for(int i=0;i<j;i++){
                    voucherList.add("50rb");
                }
                poin = poin%400;
            }
            
            if(poin>=200){
                int j = poin/200;
                for(int i=0;i<j;i++){
                    voucherList.add("25rb");
                }
                poin = poin%200;
            }
            
            if(poin>=100){
                int j = poin/100;
                for(int i=0;i<j;i++){
                    voucherList.add("10rb");
                }
                poin = poin%100;
            }
            
        }
        
        
        System.out.println("voucher yang di redeem: " + voucherList);
        System.out.println("sisa poin: "+poin+"p");
    }
}
